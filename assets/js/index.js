// Original date
//const end = new Date('9/25/2021 5:00 PM');
const end = new Date('9/25/2026 5:00 PM');
const _second = 1000;
const _minute = _second * 60;
const _hour = _minute * 60;
const _day = _hour * 24;
const timer = 0 ;

function showRemaining() {
    const now = new Date();
    const distance = end - now;
    if (distance < 0) {
        clearInterval(timer);
        document.getElementById('countdown').innerHTML = 'El tiempo ya pasó!';
        return;
    }
    const days = Math.floor(distance / _day);
    const hours = Math.floor((distance % _day) / _hour);
    const minutes = Math.floor((distance % _hour) / _minute);
    const seconds = Math.floor((distance % _minute) / _second);
    document.getElementById('countdown').innerHTML = days + ' DÍAS, ';
    document.getElementById('countdown').innerHTML += hours + ' HORAS, ';
    document.getElementById('countdown').innerHTML += minutes + ' MINUTOS Y ';
    document.getElementById('countdown').innerHTML += seconds + ' SEGUNDOS';
}

timer = setInterval(showRemaining, 1000);


//UP BUTTON
mybutton = document.getElementById("myBtn");
mybutton.addEventListener("click",()=>{
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
})
