let carrito = [];
let cantCarrito = 0;
let depTotal = 0;
let listadoProducto = []

const contenedorProductos = document.getElementById('cards')
const contenedorProductosB = document.getElementById('cardsB')
const count = document.getElementById('count_cant')


const render = async () => {
    const resp = await fetch("../json/productos.json")
    const data = await resp.json()
    const listadoIndonesia = data.filter( item => item.pais ==='INDONESIA')
    const listadoEspanha = data.filter( item => item.pais ==='ESPANHA')
    listadoProducto = data
    /* ORDEN X ID */
    listadoIndonesia.sort(function (a, b) {
        return a.id - b.id;
    });
    listadoEspanha.sort(function (a, b) {
        return a.id - b.id;
    });

    listadoIndonesia.forEach(producto =>{
        let div = document.createElement("div");
        div.className += "col-md-6 col-lg-3 my-3";
        if (producto?.img == undefined) {
            producto.img = "../assets/img/notFound.png"
        }
        div.innerHTML = `
            <div id="${producto?.id}" class="d-flex justify-content-center">      
                <img class="gift__card" src="${producto?.img}" alt="${producto?.nombre}">
            </div>
            <p class="text-center">${producto?.nombre}</p>
            <h5 class="text-center">$ ${producto?.precio}</h5>
            <div class="d-flex justify-content-center">
                <input id="prod${producto?.id}" data-id="${producto?.id}" class="price_tag" type="button" value="Regalar">
            </div>
        `
        contenedorProductosB.appendChild(div);
        // agregar un evento al botón de agregar al listado
        const boton = document.getElementById(`prod${producto?.id}`)
        boton.addEventListener("click",() => {
            agregarCarrito(producto?.id)
        })
    })

    listadoEspanha.forEach(producto=>{
        let div = document.createElement("div");
        div.className += "col-md-6 col-lg-3 my-3";
        if (producto?.img == undefined) {
            producto.img = "../assets/img/notFound.png"
        }
        div.innerHTML = `
            <div id="${producto?.id}" class="d-flex justify-content-center">      
                <img class="gift__card" src="${producto?.img}" alt="${producto?.nombre}">
            </div>
            <p class="text-center">${producto?.nombre}</p>
            <h5 class="text-center">$ ${producto?.precio}</h5>
            <div class="d-flex justify-content-center">
                <input id="prod${producto?.id}" data-id="${producto?.id}" class="price_tag" type="button" value="Regalar">
            </div>
        `
        contenedorProductos.appendChild(div)
        // agregar un evento al botón de agregar al listado
        const boton = document.getElementById(`prod${producto?.id}`)
        boton.addEventListener("click",() => {
            agregarCarrito(producto?.id)
        })
    })

    let agregarCarrito = id => {
        producto = listadoProducto.find(producto => producto.id == id )
        if (producto == undefined ){
            console.log('Ingrese un numero valido')
        } else {
            console.log("carrito",carrito)
            if( ! carrito.includes(producto)){
                carrito.push(producto);
                //Agrego al LocalStorage:
                localStorage.setItem("carrito", JSON.stringify(carrito));
                cantCarrito ++;
                localStorage.setItem("cantidad", cantCarrito);
                depTotal += producto.precio;
                localStorage.setItem("total", depTotal);
                Toastify({
                    text: `Agregaste ${producto.nombre} a tu lista 😁`,
                    duration: 3000,
                    close: true,
                    gravity: "top",
                    stopOnFocus: true, 
                    style: {
                        fontFamily: 'Antic Didone, serif',
                        //background: "linear-gradient(to right, #928C90, #C79C96 )",
                        background: "#C79C96",
                    },
                }).showToast();
                console.log("Has agregado un nuevo producto a tu carrito 😁")
            } else {console.log("Este producto ya está en tu carrito 😊")}
        }
        count.innerHTML =cantCarrito
        readFooter()
        readCarrito()
        
    
    }

    let quitarCarrito = id => {
        producto = listadoProducto.find(producto => producto?.id == id )
        if (producto == undefined ){
            console.log('Ingrese un numero valido')
        } else {
            if(! carrito.includes(producto)){
                console.log("El producto no se encuentra en tu carrito 😮")
            } else {
                carrito.pop(producto);
                localStorage.setItem("carrito", JSON.stringify(carrito));
                cantCarrito --;
                localStorage.setItem("cantidad", cantCarrito);
                depTotal -= producto.precio;
                localStorage.setItem("total", depTotal);
                console.log("Has quitado el elemento en tu carrito 😁")
            }
        };
        count.innerHTML =cantCarrito;
        readFooter();
    }

    
    let limpiarCarrito = () => {
        carrito = [];
        cantCarrito = 0;
        depTotal =0;
        localStorage.setItem("carrito", JSON.stringify(carrito));
        localStorage.setItem("cantidad", cantCarrito);
        localStorage.setItem("total", depTotal);
        console.log("Su carrito está vacío 😁")
        count.innerHTML =cantCarrito;
        readFooter();
    }

    //UP BUTTON
    mybutton = document.getElementById("myBtn");
    mybutton.addEventListener("click",()=>{
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    })

    /* footer table constructor */
    const footer = document.getElementById('footer')
    const templateFooter = document.getElementById('template-footer').content
    const templateCarrito = document.getElementById('template-carrito').content
    const fragment = document.createDocumentFragment()


    const readCarrito = () => {
        items.innerHTML = ''
        Object.values(carrito).forEach(producto => {
            templateCarrito.querySelectorAll('td')[0].textContent = producto?.nombre
            templateCarrito.querySelector('span').textContent = producto?.precio
            const clone = templateCarrito.cloneNode(true)
            fragment.appendChild(clone)

        })
        items.appendChild(fragment)

        readFooter()
    }


    const readFooter = () =>{
        footer.innerHTML = ''
        if (cantCarrito === 0) {
            footer.innerHTML = `
            <th scope="row" colspan="5">Carrito vacío </th>
            `
            return
        }
        
        total = document.getElementById('valor_total')
        total.innerHTML = depTotal
        templateFooter.querySelector('span').textContent = depTotal;

        const clone = templateFooter.cloneNode(true)
        fragment.appendChild(clone)
        footer.appendChild(fragment)

        const hollowBoton = document.querySelector('#vaciar-carrito')
        hollowBoton.addEventListener('click', () => {
            limpiarCarrito()
            readCarrito()
        })
    }


    if(localStorage.getItem("carrito")) {
        let localCarrito = JSON.parse(localStorage.getItem("carrito"));
        let localCantCarrito = localStorage.getItem("cantidad");
        let localDepTotal = localStorage.getItem("total");
        localCarrito.forEach((producto) => {
            producto = listadoProducto.find(el=> el.id == producto.id);
            carrito.push(producto);
        })
        cantCarrito = parseInt(localCantCarrito);
        depTotal = parseInt(localDepTotal);
        count.innerHTML =cantCarrito
        readCarrito()
        readFooter()
    }
}
render()














