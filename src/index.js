let carrito = [];
let cantCarrito = 0;
let depTotal = 0;

class Producto {
    constructor(id,nombre, precio) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
    }
}

const helado = new Producto(1,"Helado",1000);
const paseo = new Producto(2,"Paseo",2000);
const viaje = new Producto(3,"Viaje",500000);
const cena = new Producto(4,"Cena",40000);
const fiesta = new Producto(5,"Fiesta",20000);


let listadoProducto = [helado,paseo,viaje,cena,fiesta];


let agregarCarrito = id => {
    producto = listadoProducto.find(producto => producto.id == id )
    if (producto == undefined ){
        console.log('Ingrese un numero valido')
    } else {
        if( ! carrito.includes(producto)){
            carrito.push(producto);
            cantCarrito += 1;
            depTotal += producto.precio;
            console.log("Has agregado un nuevo producto a tu carrito 😁")
        } else {console.log("Este producto ya está en tu carrito 😊")}
    }
}


let quitarCarrito = id => {
    producto = listadoProducto.find(producto => producto.id == id )
    if (producto == undefined ){
        console.log('Ingrese un numero valido')
    } else {
        if(! carrito.includes(producto)){
            console.log("El producto no se encuentra en tu carrito 😮")
        } else {
            carrito.pop(producto);
            cantCarrito -= 1;
            depTotal -= producto.precio;
            console.log("Has quitado el elemento en tu carrito 😁")
        }
    }
}

let limpiarCarrito = () => {
    carrito = [];
    cantCarrito = 0;
    console.log("Su carrito está vacío 😁")
}

console.log("Escriba 'salir' para terminar el programa ❗❗❗")
console.log("Listado de productos 🔖")
for(el of listadoProducto) {console.log('ID :',el.id,',',el.nombre,', precio: $',el.precio)}



do {
    //console.clear();
    idProducto = prompt('Insrese el id del producto que quiere ingresar al carro: ')
    if (idProducto == undefined) {
        console.log('Debe ingresar un id');
        continue;
    } else if (idProducto != 'salir') {
        if ( isNaN(idProducto) ){
            console.log('Debe ingresar un número valido 🤦');
            continue;
        }
        idProducto = parseFloat(idProducto);
        agregarCarrito(idProducto)
    } else {
        console.log('Ha finalizado el ingreso de productos 🥳');
        console.log('Items en carrito:'+cantCarrito)
        console.log('Valor total: '+depTotal)
    }
} while (idProducto !== 'salir' && idProducto !== undefined);



if ( carrito.length !== 0) {
    console.log('Listado de productos en tu carro:')
    for(el of carrito) {console.log('ID :',el.id,',',el.nombre,', precio: $',el.precio)}
    console.log('Valor total: '+depTotal)
    console.log('Items en carrito:'+cantCarrito)

} else {
    console.log('No ha ingresado productos en su carrito')
}
